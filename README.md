EJERCICIOS DE AYUDANTÍAS
---
Ejercicios para prácticar el desarrollo de aplicaciones web con Reactjs.

## Obtener
```
git clone --recursive https://gitlab.com/desafiolatam-reactjs-assistantships/exercises.git
```

## Listado de ejercicios
- [Componentes](https://gitlab.com/desafiolatam-reactjs-assistantships/1.components)
- [Comparar productos](https://gitlab.com/desafiolatam-reactjs-assistantships/2.compare-products)
- [Canasta virtual](https://gitlab.com/desafiolatam-reactjs-assistantships/3.shopping-cart)
- [Pokedex](https://gitlab.com/desafiolatam-reactjs-assistantships/4.pokedex)
- [Pronóstico del tiempo](https://gitlab.com/desafiolatam-reactjs-assistantships/5.weather-forecast)
- [Buscador de Spotify](https://gitlab.com/desafiolatam-reactjs-assistantships/6.spotify-search)

## Recursos
- [Documentación oficial de React.js](https://reactjs.org/docs)
- [Documentación oficial de Redux.js](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
